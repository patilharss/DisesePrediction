package com.example.diseaseprediction;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.res.AssetFileDescriptor;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.tensorflow.lite.Interpreter;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class DiabitesActivity extends AppCompatActivity {
    EditText preg,glucose,bp,st,insulin,bmi,dpf,age;

    TextView textview;
    Button pred;

    Interpreter interpreter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diabites);
        setTitle("Diabites");
        // for going back to previous activity
        //by defining the parent activity to this activity in AndroidManifest.xml
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        //Initializing 

        try {
            interpreter = new Interpreter(loadModelFile(),null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        preg=findViewById(R.id.preg);
        glucose=findViewById(R.id.glucose);
        bp=findViewById(R.id.bp);
        st=findViewById(R.id.st);
        insulin=findViewById(R.id.insulin);
        bmi=findViewById(R.id.bmi);
        dpf=findViewById(R.id.dpf);
        age=findViewById(R.id.age);


        textview=findViewById(R.id.textView);
        pred=findViewById(R.id.button);



        //----------------code for Diabetes prediction-----------------

        pred.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if (preg.getText().toString().matches("")) {
                    Toast.makeText(DiabitesActivity.this,"please fill all required feilds!",Toast.LENGTH_SHORT).show();
                    return;
                }
                else {

                    float f = doInference(preg.getText().toString(), glucose.getText().toString(), bp.getText().toString(), st.getText().toString(), insulin.getText().toString(), bmi.getText().toString(), dpf.getText().toString(), age.getText().toString());
                    textview.setText("Probability that you are diabetic is: " + f + "%");

                    if (f > 0.5) {
                        textview.setTextColor(getResources().getColor(R.color.design_default_color_error));

                    } else {

                        textview.setTextColor(getResources().getColor(R.color.purple_700));
                    }
                }

            }


        });

    }
    public float doInference(String preg,String glucose,String bp,String st,String insulin,String bmi,String dpf,String age )
    {
        float[][] input = new float[1][8];
        input[0][0] = Float.parseFloat(preg);
        input[0][1] = Float.parseFloat(glucose);
        input[0][2] = Float.parseFloat(bp);
        input[0][3] = Float.parseFloat(st);
        input[0][4] = Float.parseFloat(insulin);
        input[0][5] = Float.parseFloat(bmi);
        input[0][6] = Float.parseFloat(dpf);
        input[0][7] = Float.parseFloat(age);



        float[][] output = new float[1][1];
        interpreter.run(input,output);
        return output[0][0];
    }

//to load file.


    private MappedByteBuffer loadModelFile() throws IOException
    {
        AssetFileDescriptor assetFileDescriptor =    			this.getAssets().openFd("diabetes.tflite");
        FileInputStream fileInputStream = new 		FileInputStream(assetFileDescriptor.getFileDescriptor());
        FileChannel fileChannel = fileInputStream.getChannel();
        long startOffset = assetFileDescriptor.getStartOffset();
        long length = assetFileDescriptor.getLength();
        return         fileChannel.map(FileChannel.MapMode.READ_ONLY,startOffset,length);
    }





}
