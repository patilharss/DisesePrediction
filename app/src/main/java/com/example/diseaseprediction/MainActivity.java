package com.example.diseaseprediction;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button diabetesbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        diabetesbutton= (findViewById(R.id.diabetesButton));

        //Onclick listener for Diabites Button
        diabetesbutton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent diabetesIntent=new Intent(MainActivity.this,DiabitesActivity.class);
                startActivity(diabetesIntent);

            }


        });
        
        
        
        
        
    }
}